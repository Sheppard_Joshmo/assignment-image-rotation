#include "bmp.h"
#include <stdbool.h>

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

bool rotate(const struct image* old, struct image* new);
void image_clear(struct image i);

#endif
