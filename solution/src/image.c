#include "image.h"

bool rotate(const struct image* old, struct image* const new){
    struct image temp = image_create_struct(old->width, old->height);
    if (temp.data == NULL){
        return false;
    }
    *new = temp;
    for (size_t i = 0; i < old->height; i++){
        for (size_t j = 0; j < old->width; j++){
            new->data[old->height * j + (old->height - 1 - i)] = old->data[old->width * i + j];
        }
    }
    return true;
}

void image_clear(struct image i){
    free(i.data);
}
