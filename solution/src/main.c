#include "image.h"
#include "status.h"
#include <stdio.h>

static void files_close(FILE* f1, FILE* f2){
    fclose(f1);
    fclose(f2);
}

static bool handle_read_errors(enum status st, FILE* f1, FILE* f2){
    if (st == SUCCESS){
        fprintf(stdout, "%s", "File was read\n");
    } else if (st == FILE_READ_HEADER_ERROR) {
        fprintf(stderr, "%s", "Problem with reading header from file\n");
        files_close(f1, f2);
        return true;
    } else if (st == FILE_READ_IMAGE_ERROR) {
        fprintf(stderr, "%s", "Problem with reading image from file\n");
        files_close(f1, f2);
        return true;
    }
    return false;
}

static bool handle_write_errors(enum status st,FILE* f1, FILE* f2){
    if (st == SUCCESS){
        fprintf(stdout, "%s", "File was write\n");
    } else if (st == FILE_WRITE_HEADER_ERROR) {
        fprintf(stderr, "%s", "Problem with writing header\n");
        files_close(f1, f2);
        return true;
    } else if (st == FILE_WRITE_IMAGE_ERROR) {
        fprintf(stderr, "%s", "Problem with writing image\n");
        files_close(f1, f2);
        return true;
    }
    return false;
}

int main( int argc, char** argv ) {
//    (void) argc; (void) argv; // suppress 'unused parameters' warning

    if (argc != 3){
        fprintf(stderr, "%s", "Wrong number of arguments\n");
    }
    FILE* old_img_file = fopen(argv[1], "rb");
    if (old_img_file == NULL){
        fprintf(stderr, "Can't open file %s\n", argv[1]);
        return 1;
    }

    FILE* new_img_file = fopen(argv[2], "wb");
    if (new_img_file == NULL){
        fprintf(stderr, "Can't open file %s\n", argv[2]);
        return 1;
    }

    struct image old = {0};
    struct image new = {0};
    enum status temp_status = {0};

    temp_status = bmp_read(old_img_file, &old);
    if (handle_read_errors(temp_status, old_img_file, new_img_file)){
        return 1;
    }

    if (rotate(&old, &new)) {
        fprintf(stdout, "%s", "Image rotated\n");
    } else {
        fprintf(stdout, "%s", "Problem with image rotating\n");
        return 1;
    }

    temp_status = bmp_write(new_img_file, &new);
    if (handle_write_errors(temp_status, old_img_file, new_img_file)){
        return 1;
    }

    image_clear(old);
    image_clear(new);
    files_close(old_img_file, new_img_file);
    return 0;
}
