#include "bmp.h"

#define HEADER_TYPE 19778
#define HEADER_BIT_COUNT 24
#define HEADER_COMPRESSION 0
#define HEADER_SIZE 40

static uint32_t padding_get(const struct image* const i){
    return i->width % 4;
}

static uint32_t image_get_size(const struct image* const i){
    return i->height * i->width * sizeof(struct pixel) + padding_get(i)*i->height;
}

static enum status header_read(FILE* f, struct bmp_header* h){
    if (fread(h, sizeof(struct bmp_header), 1, f) != 1){
        return FILE_READ_HEADER_ERROR;
    }
    if (h->bfType != HEADER_TYPE || h->biBitCount != HEADER_BIT_COUNT || h->biCompression != HEADER_COMPRESSION || h->biSize != HEADER_SIZE) {
        return FILE_READ_HEADER_ERROR;
    }
    return SUCCESS;
}

static struct bmp_header header_create(const struct image* const i){
    struct bmp_header result = {0};

    result.biClrImportant = 0;
    result.bfType = 19778;
    result.biPlanes = 1;
    result.biBitCount = 24;
    result.bfReserved = 0;
    result.biSize = 40;
    result.biYPelsPerMeter = 0;
    result.biClrUsed = 0;
    result.biCompression = 0;
    result.biXPelsPerMeter = 0;
    result.biWidth = i->width;
    result.biHeight = i->height;
    result.biSizeImage = image_get_size(i);
    result.bOffBits = sizeof(struct bmp_header);
    result.bfileSize = result.biSizeImage + result.bOffBits;
    return result;
}

static enum status resource_read(FILE* const f, uint32_t h, uint32_t w, struct image* const img){
    struct image create_template = image_create_struct(h, w);
    if (create_template.data != NULL){
        *img = create_template;
    } else {
        return FILE_READ_IMAGE_ERROR;
    }
    for (uint32_t i = 0; i < h; i++){
        if (fread(&(img->data[i*w]), sizeof(struct pixel), w, f) != w){
            return FILE_READ_IMAGE_ERROR;
        }
        if (fseek(f, padding_get(img), SEEK_CUR) != 0){
            return FILE_READ_IMAGE_ERROR;
        }
    }
    return SUCCESS;
}

struct image image_create_struct(uint32_t h, uint32_t w){
    struct image result = {0};
    result.height = h;
    result.width = w;
    result.data = malloc(sizeof(struct pixel) * h * w);
    return result;
}

enum status bmp_read(FILE* const old_file, struct image* old_img){

    struct bmp_header header = {0};
    enum status temp_result = {0};
    temp_result = header_read(old_file, &header);
    if (temp_result != SUCCESS){
        return temp_result;
    }

    if (header.biWidth > 0 && header.biHeight > 0){
        resource_read(old_file, header.biHeight, header.biWidth, old_img);
    } else {
        return FILE_READ_IMAGE_ERROR;
    }

    return SUCCESS;
}

enum status bmp_write(FILE* const new_file, const struct image* const new_img){
    struct bmp_header h = header_create(new_img);
    if (fwrite(&h, sizeof(struct bmp_header), 1, new_file) != 1) return FILE_WRITE_HEADER_ERROR;
    uint16_t temp[3] = {0};
    for (uint32_t i = 0; i < new_img->height; i++){
        if (fwrite(&(new_img->data[i * new_img->width]),
                   sizeof(struct pixel),new_img->width, new_file) == 0) return FILE_WRITE_IMAGE_ERROR;
        if (fwrite(temp, padding_get(new_img), 1, new_file) == 0) return FILE_WRITE_IMAGE_ERROR;
    }
    return SUCCESS;
}
